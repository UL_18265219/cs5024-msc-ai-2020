Michel Danjou
Student id: 18263461


## Files
  * *Strategy*: This is the main strategy file. 
  The aim is to favor corners and edges first, and then to de-prioritize cells that are under threat. 
  The intuition is that we probably shouldn't play cells that are about to be taken by the enemy and we should instead fortify our position towards the center of the board.
  * *subSub_take5*: this is the process described on slide "Hierachy - The Sub AI"

**Note:**

I have add multiple issues with the Eclipse application. See forum.
I am posting something very basic because this is all I can get to work at present.
